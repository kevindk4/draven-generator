<?php

if(!function_exists('get_template')){
    function get_template($name){
        $basePath = base_path("vendor/draven/generator/src/template/");
        $path = $basePath.$name.".stub";
        return file_get_contents($path); 
    }
}

if(!function_exists('create_directory')){
    function create_directory($path){
        if (!file_exists($path)) {
            mkdir($path, 0755, true);
        }
    }
}

if(!function_exists('create_file')){
    function create_file($path,$fileName,$content){
        if (!file_exists($path)) {
            mkdir($path, 0755, true);
        }

        $path = $path.$fileName;

        file_put_contents($path, $content);
    }
}