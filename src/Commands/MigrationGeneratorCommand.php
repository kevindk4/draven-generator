<?php

namespace Draven\Generator\Commands;

use Draven\Generator\Commands\Generators\MigrationGenerator;
use Illuminate\Console\Command;

class MigrationGeneratorCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'draven:migration {model : Model Name} {--primary= : Custom primary key, will be created as auto-increment integer}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a database migration';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = $this->argument('model');
        $customPrimary = $this->option('primary');
        $inputs = [];
        $soft_delete = false;

        if ($this->confirm("Do you want this model to use soft delete?", true)) {
            $soft_delete = true;
        }

        while (true) {
            $dataPush = [];

            //For Field
            $field = $this->ask('Enter field name or "exit" to exit the command');
            if ($field == '') {
                $this->error('Field can not be an empty string!');
                continue;
            }
            if ($field == "exit") break;

            //For Database Type
            $dbType = $this->ask("Enter field database type ex:\"integer\",\"string,25\",\"double,8,2\",etc.\n (ref : https://laravel.com/docs/8.x/migrations#available-column-types)");
            if (strtolower($dbType) == 'enum' || strtolower($dbType) == 'set') {
                $extra = $this->ask("Enter $dbType fields ex:\"easy,hard\"");

                if ($extra == '') {
                    $this->error("Extra can not be empty!");
                    continue;
                }
            }

            if ($dbType == '') {
                $this->error('Database type can not be empty!');
                continue;
            }

            //Database options
            $this->comment("Leave empty if not used");
            $dbOptions = $this->ask("Enter modifier for the database type , ex: \"unique,unsigned,charset('utf8mb4'),default('defaultVal')\"\n (ref : https://laravel.com/docs/8.x/migrations#column-modifiers)");


            if (isset($field) && isset($dbType)) {
                $dataPush['fieldName'] = $field;
                $dataPush['dbType'] = $dbType;
                if (isset($extra)) {
                    $dataPush['extra'] = explode(",", $extra);
                }
                if (isset($dbOptions)) {
                    $dataPush['dbOptions'] = explode(",", $dbOptions);
                }
            }

            //Relationship
            if ($this->confirm("Do you want to add relationship?")) {
                while (true) {
                    $this->comment("Type \"end\" to exit relation");
                    $relationType = $this->ask("1t1 : 1 to 1,\n mt1 : many to 1, \n mtm : many to many\n Relation type from this model to another model :");
                    if ($relationType == "end") {
                        break;
                    }
                    if ($relationType != "1t1" && $relationType != "mt1" && $relationType != "1tm" && $relationType != "mtm") {
                        $this->error("Unrecognized relation type, skipping relationship.");
                        continue;
                    }
                    $modelName = $this->ask("Enter the foreign model name : ex:\"ModelName\"");
                    $oModelPrimary = $this->ask("Enter foreign model primary key (leave empty if the primary key is id) :");
                    if (isset($relationType) && isset($modelName)) {
                        $relation[] = (object)[
                            'type' => $relationType,
                            'modelName' => $modelName
                        ];
                        $lastObject = $relation[sizeof($relation) - 1];
                        //Handle Custom Foreign Primary
                        if (isset($oModelPrimary)) {
                            $lastObject->primary = $oModelPrimary;
                        }
                        $dataPush['relation'] = $relation;
                    } else {
                        $this->error("One of the input is empty, skipping relationship");
                    }
                }


                array_push($inputs, (object)$dataPush);
            }
        }

        $migrationGenerator = new MigrationGenerator($this);
        $migrationGenerator->generate($name, $inputs, $soft_delete, $customPrimary);
    }
}
