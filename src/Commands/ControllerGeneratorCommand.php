<?php

namespace Draven\Generator\Commands;

use Draven\Generator\Commands\Generators\ControllerGenerator;
use Illuminate\Console\Command;

class ControllerGeneratorCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'draven:controller {model : Model Name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a controller class';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $modelName = $this->argument('model');
        //Controller Generator
        $controllerGenerator = new ControllerGenerator($this);
        $controllerGenerator->generate($modelName);
    }
}
