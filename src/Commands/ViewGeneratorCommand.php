<?php

namespace Draven\Generator\Commands;

use Draven\Generator\Commands\Generators\RequestGenerator;
use Draven\Generator\Commands\Generators\ViewGenerator;
use Illuminate\Console\Command;

class ViewGeneratorCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'draven:view {model : Model Name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a view';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = $this->argument('model');
        $inputs = [];
        while (true) {
            $dataPush = [];

            //For Field
            $field = $this->ask('Enter field name or "exit" to exit the command');
            if ($field == '') {
                $this->error('Field can not be an empty string!');
                continue;
            }
            if ($field == "exit") break;

            //For HTML Type
            $HTMLtype = $this->ask('Enter html type ex: text,number,checkbox,etc. (ref:https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input)');
            if ($HTMLtype == '') {
                $this->error('HTML Type can not be empty!');
                continue;
            } 

            if (isset($field)) {
                $dataPush['fieldName'] = $field;
                $dataPush['htmlType'] = $HTMLtype;
            }
            array_push($inputs, (object)$dataPush);
        }


        $viewGenerator = new ViewGenerator($this);
        $viewGenerator->generate($name, $inputs);
    }
}
