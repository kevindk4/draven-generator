<?php

namespace Draven\Generator\Commands;

use Draven\Generator\Commands\Generators\RepositoryGenerator;
use Illuminate\Console\Command;

class RepositoryGeneratorCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'draven:repository {model : Model Name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a repository class';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = $this->argument('model');
        $inputs = [];

        while (true) {
            $dataPush = [];

            //For Field
            $field = $this->ask('Enter field name or "exit" to exit the command');
            if ($field == '') {
                $this->error('Field can not be an empty string!');
                continue;
            }
            if ($field == "exit") break;
            
            if (isset($field)) {
                $dataPush['fieldName'] = $field;
            }


            //Relationship
            if ($this->confirm("Do you want to add relationship?")) {
                while (true) {
                    $this->comment("Type \"end\" to exit relation");
                    $relationType = $this->ask("1t1 : 1 to 1,\n mt1 : many to 1, \n mtm : many to many\n Relation type from this model to another model :");
                    if($relationType == "end"){
                        break;
                    }
                    if ($relationType != "1t1" && $relationType != "mt1" && $relationType != "1tm" && $relationType != "mtm") {
                        $this->error("Unrecognized relation type, skipping relationship.");
                        continue;
                    }
                    $modelName = $this->ask("Enter the foreign model name : ex:\"ModelName\"");
                    $oModelPrimary = $this->ask("Enter foreign model primary key (leave empty if the primary key is id) :");
                    if (isset($relationType) && isset($modelName)) {
                        $relation[] = (object)[
                            'type' => $relationType,
                            'modelName' => $modelName
                        ];
                        //Handle Append
                        $lastObject = $relation[sizeof($relation)-1];
                        if ($relationType == "1t1" || $relationType == "mt1") {
                            if ($this->confirm("Do you want to use append?")) {
                                while (true) {
                                    $this->comment("Type \"end\" to exit append");
                                    $attrName = $this->ask("Enter foreign table attribute name");
                                    if ($attrName == "end") {
                                        break;
                                    }
                                    if (isset($attrName)) {
                                        $lastObject->append[] = $attrName;
                                    } else {
                                        $this->error("foreign table attribute name can not be empty!");
                                        continue;
                                    }
                                }
                            }
                        }
                        //Handle Custom Foreign Primary
                        if (isset($oModelPrimary)) {
                            $lastObject->primary = $oModelPrimary;
                        }
                        $dataPush['relation'] = $relation;
                    } else {
                        $this->error("One of the input is empty, skipping relationship");
                    }
                }
            }

            array_push($inputs, (object)$dataPush);
        }

        //Repository Generator
        $repositoryGenerator = new RepositoryGenerator($this);
        $repositoryGenerator->generate($name, $inputs);
    }
}
