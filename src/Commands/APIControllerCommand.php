<?php

namespace Draven\Generator\Commands;

use Draven\Generator\Commands\Generators\APIControllerGenerator;
use Illuminate\Console\Command;

class APIControllerCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'draven:api.controller {model : Model Name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate an API controller class';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = $this->argument('model');
        //Controller Generator
        $controllerGenerator = new APIControllerGenerator($this);
        $controllerGenerator->generate($name);
    }
}
