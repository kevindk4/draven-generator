<?php

namespace Draven\Generator\Commands;

use Illuminate\Console\Command;

class CRUDGeneratorCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'draven:generate {model : Model Name} {--primary= : Custom primary key, will be created as auto-increment integer} {--fromFile= : Load model field from a JSON file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a basic CRUD operation';

    /**
     * Composer
     * 
     * @var Composer
     */
    private $composer;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->composer = app()['composer'];
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = $this->argument('model');
        $customPrimary = $this->option('primary');
        $fromFile = $this->option('fromFile');

        //Generate the login ui
        if (!file_exists(resource_path('/views/auth'))) {
            $this->call('ui', ['type' => 'vue', '--auth' => true]);
        }

        //Generate The Model
        if (isset($customPrimary) && isset($fromFile)) {
            $this->call('draven:model', ['model' => $name, '--primary' => $customPrimary, '--fromFile' => $fromFile, '--view' => true, '--repository' => true, '--migration' => true, '--route' => true]);
        } else if (isset($customPrimary)) {
            $this->call('draven:model', ['model' => $name, '--primary' => $customPrimary, '--view' => true, '--repository' => true, '--migration' => true, '--route' => true]);
        } else if (isset($fromFile)) {
            $this->call('draven:model', ['model' => $name, '--fromFile' => $fromFile, '--view' => true, '--repository' => true, '--migration' => true, '--route' => true]);
        } else {
            $this->call("draven:model", ['model' => $name, '--view' => true, '--repository' => true, '--migration' => true, '--route' => true]);
        }

        //Generate The Controller
        $this->call('draven:controller', ['model' => $name]);

        //Generate The Request
        $this->call('draven:request', ['model' => $name]);

        $this->info("\nCode Generated Successfully.\n");

        $this->info('Generating autoload files');
        $this->composer->dumpOptimized();

        if ($this->confirm("Do you want to run fresh migration?")) {
            $this->call('migrate:fresh');
        }
    }
}
