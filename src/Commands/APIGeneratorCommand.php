<?php

namespace Draven\Generator\Commands;

use Illuminate\Console\Command;

class APIGeneratorCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'draven:api {model : Model Name} {--primary= : Custom primary key, will be created as auto-increment integer} {--fromFile= : Load model field from a JSON file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate an API CRUD operation';

    /**
     * Composer
     * 
     * @var Composer
     */
    private $composer;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->composer = app()['composer'];
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = $this->argument('model');
        $customPrimary = $this->option('primary');
        $fromFile = $this->option('fromFile');

        //Generate The Model
        if (isset($customPrimary) && isset($fromFile)) {
            $this->call('draven:model', ['model' => $name, '--primary' => $customPrimary, '--fromFile' => $fromFile, '--repository' => true, '--migration' => true, '--route' => true, '--api' => true]);
        } else if (isset($customPrimary)) {
            $this->call('draven:model', ['model' => $name, '--primary' => $customPrimary, '--repository' => true, '--migration' => true, '--route' => true, '--api' => true]);
        } else if (isset($fromFile)) {
            $this->call('draven:model', ['model' => $name, '--fromFile' => $fromFile, '--repository' => true, '--migration' => true, '--route' => true, '--api' => true]);
        } else {
            $this->call("draven:model", ['model' => $name, '--repository' => true, '--migration' => true, '--route' => true, '--api' => true]);
        }

        //Generate The Controller
        $this->call('draven:api.controller', ['model' => $name]);

        //Generate The Request
        $this->call('draven:request', ['model' => $name]);

        $this->info("\nCode Generated Successfully.\n");

        $this->info('Generating autoload files');
        $this->composer->dumpOptimized();

        if ($this->confirm("Do you want to run fresh migration?")) {
            $this->call('migrate:fresh');
        }
    }
}
