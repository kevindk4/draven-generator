<?php

namespace Draven\Generator\Commands;

use Draven\Generator\Commands\Generators\RequestGenerator;
use Illuminate\Console\Command;

class RequestGeneratorCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'draven:request {model : Model Name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a request class';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $modelName = $this->argument('model');
        //Request Generator
        $requestGenerator = new RequestGenerator($this);
        $requestGenerator->generate($modelName);
    }
}
