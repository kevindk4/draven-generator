<?php
namespace Draven\Generator\Commands\Generators;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Str;


class RequestGenerator
{

    //* Command object */
    private $command;

    //Accept a command parameter
    public function __construct(Command $command)
    {
        $this->command = $command;
    }

    public function generate($name)
    {
        //Check if model is available
        $modelName = Str::ucfirst($name);
        if (!file_exists(app_path("Models/$modelName.php"))) {
            $this->command->call("draven:model", ['model' => $name]);
        }
        $template = get_template("Request");
        try {
            //Fill the template
            $result = $this->fillTemplate($template, $name);
            $this->createFile($name,$result);
        } catch (Exception $e) {
            $this->command->error($e->getMessage());
            exit;
        }
    }

    private function createFile($name, $template)
    {
        $nameUCFirst = Str::ucfirst($name);
        //Check if Requests folder is exist
        if (!file_exists(app_path("Http/Requests"))) {
            create_directory(app_path("Http/Requests"));
        }
        $fileName = $nameUCFirst."Request.php";
        create_file(app_path("/Http/Requests/"),$fileName,$template);
        $this->command->comment("Request $nameUCFirst" . "Request.php Created");
    }

    private function fillTemplate($template, $name)
    {
        return str_replace(
            '{{modelName}}',
            Str::ucfirst($name),
            $template
        );
    }
}