<?php

namespace Draven\Generator\Commands\Generators;

use Illuminate\Console\Command;
use Illuminate\Support\Str;


class ViewGenerator
{

    //* Command object */
    private $command;

    //Accept a command parameter
    public function __construct(Command $command)
    {
        $this->command = $command;
    }

    public function generate($name, $data, $customPrimary = null)
    {
        //Checks if model is available
        $modelName = Str::ucfirst($name);
        if (!file_exists(app_path("Models/$modelName.php"))) {
            $this->command->call("draven:model", ['model' => $name]);
        }

        //Generate The Folder
        $this->generateFolder($name);

        //Replace Default file
        if (!file_exists(resource_path("views/layouts/menu.blade.php"))) {
            $this->replaceDefault();
        }

        //Generate The index
        $this->generateIndex($name);

        //Generate Table
        $this->generateTable($name, $data, $customPrimary);

        //Generate Fields
        $this->generateFields($name, $data);

        //Generate Edit
        $this->generateEdit($name, $customPrimary);

        //Generate Create
        $this->generateCreate($name);

        //Generate Menu
        $this->generateMenu($name);
    }

    private function generateFolder($name)
    {
        create_directory(resource_path("views/$name"));
        create_directory(base_path("public/css"));
    }

    private function replaceDefault()
    {
        //replace layouts
        $app = get_template("view/basic/app");
        create_file(resource_path("views/layouts/"), "app.blade.php", $app);

        $bootstrap = get_template("view/basic/bootstrap");
        create_file(resource_path("views/layouts/"), "bootstrap_incl.blade.php", $bootstrap);

        $home = get_template("view/basic/home");
        create_file(resource_path("views/"), "home.blade.php", $home);

        create_file(resource_path("views/layouts/"), "menu.blade.php", "");

        $sidebar = get_template("view/basic/sidebar");
        create_file(resource_path("views/layouts/"), "sidebar.blade.php", $sidebar);

        $sidebarCSS = get_template("view/basic/sidebarCss");
        create_file(base_path("public/css/"), "sidebar.css", $sidebarCSS);
    }

    private function generateIndex($name)
    {
        $template = get_template("view/index");
        $modelName = strtolower($name);
        $modelNameUCF = Str::ucfirst($name);
        $template = str_replace(
            ['{{modelName}}', '{{modelNameUCF}}'],
            [$modelName, $modelNameUCF],
            $template
        );
        create_file(resource_path("views/$modelName/"), "index.blade.php", $template);
        $this->command->info("$modelName index.blade.php Created");
    }

    private function generateTable($name, $data, $customPrimary)
    {
        $template = get_template("view/table");
        $modelName = strtolower($name);
        $tableHeaders = "";
        $tableContent = "";
        $primaryKey = isset($customPrimary) ? $customPrimary : 'id';
        foreach ($data as $key => $var) {
            $varFieldUCF = Str::ucfirst($var->fieldName);
            $tableHeaders .= "<th>" . $varFieldUCF . "</th>";
            $tableContent .= "<td>{{\$value->" . $var->fieldName . "}}</td>";
            if (isset($var->relation)) {
                $tableHeaders .= "\n";
                $tableContent .= "\n";
                foreach ($var->relation as $key2 => $relAppend) {
                    $appends = [];
                    if(isset($relAppend->append)){
                        $appends = $relAppend->append;
                    }
                    foreach($appends as $append){
                        $appendUCF = Str::ucfirst($append);
                        $tableHeaders .= "<th>" . $appendUCF . "</th>";
                        $tableContent .= "<td>{{\$value->" . $append . "}}</td>";
                        if ($key2 < sizeof($appends) - 1) {
                            $tableHeaders .= "\n";
                            $tableContent .= "\n";
                        }
                    } 
                }
            }
            if ($key < sizeof($data) - 1) {
                $tableHeaders .= "\n";
                $tableContent .= "\n";
            }
        }
        $template = str_replace(
            ['{{modelName}}', '{{tableHeaders}}', '{{tableContent}}', '{{primaryKey}}'],
            [$modelName, $tableHeaders, $tableContent, $primaryKey],
            $template
        );
        create_file(resource_path("views/$modelName/"), "table.blade.php", $template);
        $this->command->info("$modelName table.blade.php Created");
    }

    private function generateCreate($name)
    {
        $template = get_template("view/create");
        $modelName = strtolower($name);
        $modelNameUCF = Str::ucfirst($name);
        $template = str_replace(
            ['{{modelName}}', '{{modelNameUCF}}'],
            [$modelName, $modelNameUCF],
            $template
        );
        create_file(resource_path("views/$modelName/"), "create.blade.php", $template);
        $this->command->info("$modelName create.blade.php Created");
    }

    private function generateFields($name, $data)
    {
        $fieldsInside = $this->generateFieldsInside($data);
        $template = get_template("view/fields");
        $modelName = strtolower($name);
        $template = str_replace(
            ['{{modelName}}', '{{fieldInside}}'],
            [$modelName, $fieldsInside],
            $template
        );
        create_file(resource_path("views/$modelName/"), "fields.blade.php", $template);
        $this->command->info("$modelName create.blade.php Created");
    }

    private function generateFieldsInside($data)
    {
        $replace = "";
        foreach ($data as $key => $value) {
            $template = get_template("view/fieldInside");
            $fieldName = $value->fieldName;
            $htmlType = $value->htmlType;
            $extra = '';
            if (isset($value->extra)) {
                $extraTemp = ",[";
                foreach ($value->extra as $extraCount => $extraVal) {
                    $extraTemp .= "'$extraVal'=>'$extraVal'";
                    if($extraCount < sizeof($value->extra)-1){
                        $extraTemp .= ",";
                    }
                }
                $extraTemp .= "]";

                $extra = $extraTemp;
            }
            $fieldNameUCF = Str::ucfirst($value->fieldName);
            $replace .= str_replace(
                ['{{fieldName}}', '{{fieldNameUCF}}', '{{HTML_Type}}', '{{placeHolder}}'],
                [$fieldName, $fieldNameUCF, $htmlType, $extra],
                $template
            );
            if ($key < sizeof($data) - 1) $replace .= "\n";
        }
        return $replace;
    }

    private function generateEdit($name, $customPrimary)
    {
        $template = get_template("view/edit");
        $modelName = strtolower($name);
        $modelNameUCF = Str::ucfirst($name);
        $primaryKey = isset($customPrimary) ? $customPrimary : 'id';
        $template = str_replace(
            ['{{modelName}}', '{{primaryKey}}', '{{modelNameUCF}}'],
            [$modelName, $primaryKey, $modelNameUCF],
            $template
        );
        create_file(resource_path("views/$modelName/"), "edit.blade.php", $template);
        $this->command->info("$modelName edit.blade.php Created");
    }

    private function generateMenu($name)
    {
        $template = get_template("view/basic/menuContent");
        $currMenu = file_get_contents(resource_path('/views/layouts/menu.blade.php'));
        $routeName = strtolower($name);
        $modelNameUCF = Str::ucfirst($name);
        $result = str_replace(
            ['{{modelName}}', '{{routeName}}', '{{modelNameUCF}}'],
            [$name, $routeName, $modelNameUCF],
            $template
        );
        if (!Str::contains($currMenu, $name)) {
            $currMenu .= "\n" . $result;
            create_file(resource_path("views/layouts/"), "menu.blade.php", $currMenu);
            $this->command->info("Menu Added");
        }
    }
}
