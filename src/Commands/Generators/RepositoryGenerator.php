<?php

namespace Draven\Generator\Commands\Generators;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class RepositoryGenerator
{

    //* Command object */
    private $command;

    //Accept a command parameter
    public function __construct(Command $command)
    {
        $this->command = $command;
    }

    private function generateBase()
    {
        //get template
        $data = get_template("baseRepository");
        create_directory(app_path("Repositories"));
        create_file(app_path("Repositories/"), "BaseRepository.php", $data);
    }

    public function generate($modelName, $allData = null, $customPrimary = null)
    {
        //Check if model is available
        $modelName = Str::ucfirst($modelName);
        if (!file_exists(app_path("Models/$modelName.php"))) {
            $this->command->call("draven:model", ['model' => $modelName]);
        }

        //Create a base generator
        $this->generateBase();

        $template = get_template("Repository");

        try {
            $result = $this->fillTemplate($modelName, $template, $allData, $customPrimary);
            $this->createFile($modelName, $result);
        } catch (Exception $e) {
            $this->command->error($e->getMessage());
            exit;
        }
    }

    private function createFile($name, $template)
    {
        $nameUCFirst = Str::ucfirst($name);
        create_file(app_path("Repositories/"), "{$nameUCFirst}Repository.php", $template);
        $this->command->comment("Repository $nameUCFirst" . "Repository.php Created");
    }


    private function fillTemplate($name, $template, $allData, $customPrimary)
    {
        $nameUCFirst = Str::ucfirst($name);
        //Replace model name
        $template = str_replace(
            ['{{modelName}}'],
            [$nameUCFirst],
            $template
        );

        //Generate the fillable
        $template = $this->generateFillable($template, $allData);

        if (isset($allData)) {
            //Generate the getJoin Function
            $template = $this->generateGetJoinFunctions($name, $template, $allData, $customPrimary);

            //Generate the getBy Function
            $template = $this->generateGetByFunction($template, $allData);
        }

        return $template;
    }

    private function generateFillable($template, $allData)
    {
        $fillableStr = "";
        foreach ($allData as $key => $var) {
            $fillableStr .= '\'' . $var->fieldName . '\'';
            if ($key != sizeof($allData) - 1) $fillableStr .= ",\n";
        }

        return str_replace(
            ['{{FILLABLE}}'],
            [$fillableStr],
            $template
        );
    }

    private function generateGetJoinFunctions($modelName, $template, $allData, $customPrimary)
    {
        $getJoinFunctions = "";
        $GJtemplate = get_template("GetJoinFunction");
        foreach ($allData as $dataVal) {
            $currModelUCF = Str::ucfirst($modelName);
            $currModel = strtolower($modelName);
            if (isset($dataVal->relation)) {
                foreach ($dataVal->relation as $key => $value) {
                    if($value == ""){
                        break;
                    }
                    //For Current Model Primary Key
                    if (isset($customPrimary) && $customPrimary != "") {
                        $currModelPK = $customPrimary;
                    } else {
                        $currModelPK = 'id';
                    }
                    //For Other Model Primary Key
                    if (isset($value->primary) && $value->primary != "") {
                        $oModelPK = $value->primary;
                    } else {
                        $oModelPK = 'id';
                    }
                    //For Other Model Name
                    $oModel = $value->modelName;
                    $oModelUCF = Str::ucfirst($value->modelName);
                    //For fieldName
                    $fieldName = Str::ucfirst($dataVal->fieldName);
                    $relationType = $value->type;
                    if ($relationType == "mt1" || $relationType == "mtm") {
                        $getJoinFunctions .= str_replace(
                            ['{{Cur_Model}}', '{{Cur_ModelUCF}}', '{{Other_Model}}', '{{Other_ModelUCF}}', '{{Cur_Model_PK}}', '{{Other_Model_PK}}', '{{fieldName}}'],
                            [$currModel, $currModelUCF, $oModel, $oModelUCF, $currModelPK, $oModelPK, $fieldName],
                            $GJtemplate
                        );
                    }

                    if ($key < sizeof($allData) - 1) {
                        $getJoinFunctions .= "\n\n";
                    }
                }
            }
        }

        if ($getJoinFunctions != "") {
            return str_replace(
                '{{getJoinFunction}}',
                $getJoinFunctions,
                $template
            );
        } else {
            return str_replace(
                '{{getJoinFunction}}',
                '',
                $template
            );
        }
    }

    private function generateGetByFunction($template, $allData)
    {
        $getByFunctions = "";
        $GBtemplate = get_template("GetByFunction");
        foreach ($allData as $key => $value) {
            if (isset($value->dbOptions)) {
                if(!is_array($value->dbOptions)){
                    $this->command->error("dbOptions have to be an array or null");
                    exit;
                }
                foreach ($value->dbOptions as $dbOption) {
                    if ($dbOption == "unique") {
                        $fieldName = $value->fieldName;
                        $fieldNameUF = Str::ucfirst($fieldName);
                        $getByFunctions .= str_replace(
                            ['{{Attribute_UF}}', '{{Attribute}}'],
                            [$fieldNameUF, $fieldName],
                            $GBtemplate
                        );
                        break;
                    }
                }
                if ($key < sizeof($allData) - 1) {
                    $getByFunctions .= "\n\n";
                }
            }
        }

        if ($getByFunctions != "") {
            return str_replace(
                '{{getByFunction}}',
                $getByFunctions,
                $template
            );
        } else {
            return str_replace(
                '{{getByFunction}}',
                '',
                $template
            );
        }
    }
}
