<?php
namespace Draven\Generator\Commands\Generators;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class ControllerGenerator
{

    //* Command object */
    private $command;

    //Accept a command parameter
    public function __construct(Command $command)
    {
        $this->command = $command;
    }

    public function generate($name)
    {
        $template = get_template("controller");
        try {
            $result =  $this->fillTemplate($template, $name);
            $this->createFile($name, $result);
        } catch (Exception $e) {
            $this->command->error($e->getMessage());
            exit;
        }
    }

    private function createFile($name, $template)
    {
        $nameUCFirst = Str::ucfirst($name);
        $fileName = $nameUCFirst . "Controller.php";
        create_file(app_path("/Http/Controllers/"),$fileName,$template);
        $this->command->comment("Controller $nameUCFirst" . "Controller.php Created");
    }

    private function fillTemplate($template, $name)
    {
        $nameCamel = Str::camel($name);
        $nameUCFirst = Str::ucfirst($name);
        $routeName = strtolower($name);
        return str_replace(
            [
                '{{modelName}}',
                '{{modelNameCamel}}',
                '{{routeName}}'
            ],
            [
                $nameUCFirst,
                $nameCamel,
                $routeName
            ],
            $template
        );
    }
}