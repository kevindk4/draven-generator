<?php
namespace Draven\Generator\Commands\Generators;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class APIControllerGenerator{

    //* Command object */
    private $command;

    //Accept a command parameter
    public function __construct(Command $command)
    {
        $this->command = $command;
    }

    public function generate($name){
        $template = get_template("APIController");
        //Fill the template
        try{
            $result =  $this->fillTemplate($template,$name);
            $this->createFile($name,$result);
        }catch(Exception $e){
            $this->command->error($e->getMessage());
            exit;
        }
    }

    private function createFile($name, $template){
        $nameUCFirst = Str::ucfirst($name);
        $fileName = $nameUCFirst."APIController.php";
        create_file(app_path("/Http/Controllers/"),$fileName,$template);
        $this->command->comment("Controller $nameUCFirst"."APIController.php Created");
    }

    private function fillTemplate($template, $name){
        $nameCamel = Str::camel($name);
        $nameUCFirst = Str::ucfirst($name);
        return str_replace(
            [
                '{{modelName}}',
                '{{modelNameCamel}}'
            ],
            [
                $nameUCFirst,
                $nameCamel
            ],
            $template
        );
    }
}