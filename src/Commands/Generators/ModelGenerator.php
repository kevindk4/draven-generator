<?php

namespace Draven\Generator\Commands\Generators;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class ModelGenerator
{

    //* Command object */
    private $command;

    //Accept a command parameter
    public function __construct(Command $command)
    {
        $this->command = $command;
    }

    /**
     * @var name is the name of the model
     * @var data is the input data from user e.g : field name, field type, etc.
     * @var soft_delete is if the user wants to use soft delete or not
     * @var customPrimary is if the user wants to use custom primary
     */
    public function generate($name, $data, $soft_delete, $customPrimary, $view, $migration, $repository, $route, $isAPI = false)
    {
        $template = get_template("model");
        //Fill the template
        try {
            $result = $this->fillTemplate($template, $name, $data, $soft_delete, $customPrimary);
            $this->createFile($name, $result);

            //Repository
            if ($repository) {
                $this->generateRepository($name, $data, $customPrimary);
            }

            //Migration
            if ($migration) {
                $this->generateMigration($name, $data, $soft_delete, $customPrimary);
            }

            //View
            if ($view) {
                $this->generateView($name, $data);
            }

            //Route
            if ($route) {
                $this->generateRoute($name, $isAPI);
            }
        } catch (Exception $e) {
            $this->command->error($e->getMessage());
            exit;
        }
    }

    private function createFile($name, $template)
    {
        $modelName = Str::ucfirst($name);
        $fileName = $modelName . ".php";
        create_file(app_path("Models/"), $fileName, $template);
        $this->command->comment("Model $modelName.php Created");
    }

    private function generateRepository($modelName, $inputs, $customPrimary)
    {
        $repositoryGenerator = new RepositoryGenerator($this->command);
        $repositoryGenerator->generate($modelName, $inputs, $customPrimary);
    }

    private function generateMigration($modelName, $inputs, $soft_delete, $customPrimary)
    {
        $migrationGenerator = new MigrationGenerator($this->command);
        $migrationGenerator->generate($modelName, $inputs, $soft_delete, $customPrimary);
    }

    private function generateView($name, $data)
    {
        $viewGenerator = new ViewGenerator($this->command);
        $viewGenerator->generate($name, $data);
    }

    private function generateRoute($name, $isAPI)
    {
        $routeGenerator = new RouteGenerator($this->command);
        $routeGenerator->generate($name, $isAPI);
    }

    private function fillTemplate($template, $name, $data, $soft_delete, $customPrimary)
    {
        $modelName = Str::ucfirst($name);
        $modelTemplate = str_replace(
            ['{{modelName}}'],
            [Str::ucfirst($modelName)],
            $template
        );
        //Create the fillable in model
        if (!empty($data)) {
            $modelTemplate = $this->generateFillable($modelTemplate, $data, $modelName, $soft_delete, $customPrimary);
        } else {
            $modelTemplate = str_replace(
                ['{{FILLABLE}}'],
                [''],
                $modelTemplate
            );
        }
        //Generate soft delete
        $modelTemplate = $this->generateSoftDelete($modelTemplate, $soft_delete);

        //Generate hidden
        $modelTemplate = $this->generateHidden($modelTemplate, $soft_delete);

        //Generate Custom Primary
        $modelTemplate = $this->generateCustomPrimary($modelTemplate, $customPrimary);

        //Generate Table Name
        $modelTemplate = $this->generateTableName($modelTemplate, $modelName);

        //Generate Cast
        $modelTemplate = $this->generateCast($modelTemplate, $data, $customPrimary);

        //Generate Rules
        $modelTemplate = $this->generateRules($modelTemplate, $data);

        //Generate Relationship
        $modelTemplate = $this->generateRelationship($modelTemplate, $data, $customPrimary);

        //Generate Append
        $modelTemplate = $this->generateAppend($modelTemplate, $data);

        return $modelTemplate;
    }

    private function generateFillable($template, $inputs)
    {
        $length = count($inputs);
        $fillable = "";
        foreach ($inputs as $key => $var) {
            if ($var->fieldName == "") {
                $this->command->error("fieldName can not be empty!");
                exit;
            } else {
                $fields[] = $var->fieldName;
                $fillable .= '\'' . $var->fieldName . '\'';
                if ($key != $length - 1) $fillable .= ",\n";
            }
        }

        return str_replace(
            ['{{FILLABLE}}'],
            [$fillable],
            $template
        );
    }

    private function generateSoftDelete($template, $input)
    {
        if ($input) {
            //Insert use soft delete
            $template = str_replace(
                ['{{USE_SOFT_DELETE}}'],
                ['use Illuminate\Database\Eloquent\SoftDeletes;'],
                $template
            );

            //Insert Soft delete
            $template = str_replace(
                ['{{SOFT_DELETE}}'],
                ['use SoftDeletes;'],
                $template
            );

            //Insert Deleted_at
            $template = str_replace(
                ['{{SOFT_DELETE_DATE}}'],
                ['protected $dates = [\'deleted_at\'];'],
                $template
            );
        } else {
            $template = str_replace(
                ['{{USE_SOFT_DELETE}}'],
                [''],
                $template
            );
            $template = str_replace(
                ['{{SOFT_DELETE}}'],
                [''],
                $template
            );
            $template = str_replace(
                ['{{SOFT_DELETE_DATE}}'],
                [''],
                $template
            );
        }
        return $template;
    }

    private function generateCustomPrimary($template, $customPrimary)
    {
        if (isset($customPrimary)) {
            $newPK = "protected \$primaryKey = '$customPrimary';";
            return str_replace(
                '{{Custom_Primary}}',
                $newPK,
                $template
            );
        } else {
            return str_replace(
                '{{Custom_Primary}}',
                '',
                $template
            );
        }
    }

    private function generateHidden($template, $soft_delete)
    {
        if ($soft_delete) {
            return str_replace(
                ['{{HIDDEN}}'],
                ["protected \$hidden = array('created_at', 'updated_at', 'deleted_at');"],
                $template
            );
        } else {
            return str_replace(
                ['{{HIDDEN}}'],
                ["protected \$hidden = array('created_at', 'updated_at');"],
                $template
            );
        }
    }

    private function generateTableName($template, $name)
    {
        return str_replace(
            ['{{tableName}}'],
            [strtolower($name)],
            $template
        );
    }

    private function generateCast($template, $data, $customPrimary)
    {
        $casts = '';
        //Handle if this model use custom primary key
        if (isset($customPrimary)) {
            $casts .= "'$customPrimary' => 'integer',\n";
        }
        $size = sizeof($data);
        $i = 0;
        foreach ($data as $line) {
            $casts .= "'$line->fieldName' => '$line->fieldType'";
            if ($i++ < $size - 1) {
                $casts .= ",\n";
            }
        }

        return str_replace(
            ['{{CAST}}'],
            [$casts],
            $template
        );
    }

    private function generateRules($template, $data)
    {

        $validation = '';
        $size = sizeof($data);
        $i = 0;
        foreach ($data as $line) {
            if (isset($line->validation)) {
                $lineVal = str_replace(',', '|', $line->validation);
                $validation .= "'$line->fieldName' => '$lineVal'";
                if ($i++ < $size - 1) {
                    $validation .= ",\n";
                }
            }
        }

        return str_replace(
            ['{{RULES}}'],
            [$validation],
            $template
        );
    }

    private function generateRelationship($template, $data, $customPrimary)
    {
        $relationAll = "";
        foreach ($data as $dataVal) {
            if (isset($dataVal->relation)) {
                if (!is_array($dataVal->relation)) {
                    $this->command->error("Relation have to be an array or null");
                    exit;
                }
                foreach ($dataVal->relation as $count => $value) {
                    if ($value == "") {
                        break;
                    }
                    $relationTemplate = get_template("relationship");
                    $relation = "";
                    if (isset($value->type)) {
                        $relationType = $value->type;
                        if ($relationType == "1t1") {
                            $relation = "hasOne";
                        } else if ($relationType == "mt1") {
                            $relation = "belongsTo";
                        } else if ($relationType == "1tm") {
                            $relation = "hasMany";
                        } else if ($relationType == "mtm") {
                            $relation = "belongsToMany";    
                        } else {
                            $this->command->error("Unrecognized relation type!");
                            exit;
                        }
                        if (!isset($value->modelName) || $value->modelName == "") {
                            $this->command->error("modelName can not be empty!");
                            exit;
                        }
                        $funcName = Str::camel($value->modelName);
                        $modelName = Str::ucfirst($value->modelName);
                        $location = "\\App\\Models\\" . $modelName;

                        //Replace relation template
                        $relationTemplate = str_replace(
                            ['{{relation}}', '{{Func_Name}}', '{{modelLocation}}'],
                            [$relation, $funcName, $location],
                            $relationTemplate
                        );

                        //Handle if custom primary key is used on the other model
                        if (isset($value->primary)) {
                            $fieldName = $dataVal->fieldName;
                            $primary = $value->primary == "" ? 'id' : '';
                            $PK = ",'$primary'";
                            //Handle if Append is used
                            if (isset($value->append)) {
                                $PK = ",'$fieldName'";
                            }

                            //Handle if custom primary key is used in this model
                            if (isset($customPrimary) && $customPrimary != "") {
                                $PK .= ", '$customPrimary'";
                            }

                            $relationTemplate = str_replace(
                                '{{PK}}',
                                ", $PK",
                                $relationTemplate
                            );
                        } else {
                            $fieldName = $dataVal->fieldName;
                            $PK = '';
                            //Handle if append is used
                            if (isset($value->append)) {
                                $PK = ",'$fieldName'";
                            }else{
                                $PK = ",'id'";
                            }
                            //Handle if no custom primary key is used in this model
                            if (isset($customPrimary)) {
                                $PK = ",'$customPrimary'";
                            }

                            $relationTemplate = str_replace(
                                '{{PK}}',
                                $PK,
                                $relationTemplate
                            );
                        }

                        //Check if function is already exist
                        if (!Str::contains($relationAll, $funcName)) {
                            $relationAll .= $relationTemplate;
                        }
                        if ($count < sizeof($data) - 1) {
                            $relationAll .= "\n\n";
                        }
                    }else{
                        $this->command->error("Relationship type can not be empty!");
                        exit;
                    }
                }
            }
        }
        if ($relationAll != "") {
            return str_replace(
                ['{{relation}}'],
                [$relationAll],
                $template
            );
        } else {
            return str_replace(
                ['{{relation}}'],
                [''],
                $template
            );
        }
    }


    private function generateAppend($template, $data)
    {
        $appendAll = "";
        $appendVar = "protected \$appends = [";
        foreach ($data as $dataVal) {
            if (isset($dataVal->relation)) {
                foreach ($dataVal->relation as $count => $relation) {
                    if (isset($relation->append)) {
                        if (!is_array($relation->append)) {
                            $this->command->error("Append have to be an array or null");
                            exit;
                        }
                        foreach ($relation->append as $key => $var) {
                            $size = sizeof($relation->append);
                            $appendTemplate = get_template("append");
                            $attr_Upper = Str::upper($var);
                            $attrName = $var;
                            $funcName = Str::camel($relation->modelName);
                            $appendVar .= "'$attrName'";
                            $appendAll .= str_replace(
                                ['{{Attr_Name_Uppercase}}', '{{relation}}', '{{attrName}}'],
                                [$attr_Upper, $funcName, $attrName],
                                $appendTemplate
                            );
                            if ($key < $size - 1) {
                                $appendVar .= ",";
                            }
                        }
                    }
                    if ($count < sizeof($dataVal->relation) - 1) {
                        $appendVar .= ",";
                    }
                }
            }
        }
        if ($appendAll != "") {
            $appendVar .= "];";
            return str_replace(
                ['{{Append_Var}}', '{{Append_Func}}'],
                [$appendVar, $appendAll],
                $template
            );
        } else {
            return str_replace(
                ['{{Append_Var}}', '{{Append_Func}}'],
                ['', ''],
                $template
            );
        }
    }
}
