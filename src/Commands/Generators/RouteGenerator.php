<?php

namespace Draven\Generator\Commands\Generators;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Str;


class RouteGenerator
{

    //* Command object */
    private $command;

    //Accept a command parameter
    public function __construct(Command $command)
    {
        $this->command = $command;
    }

    public function generate($name, $isAPI = false)
    {
        $routesTemplate = get_template("routes");
        $routesUseTemplate = get_template("routesUse");
        try {
            //Fill the template
            $result = $this->fillTemplate($routesTemplate, $routesUseTemplate, $name, $isAPI);
            $this->createFile($result);
        } catch (Exception $e) {
            $this->command->error($e->getMessage());
            exit;
        }
    }

    private function createFile($template)
    {
        create_file(base_path("/routes/"),"web.php",$template);
        $this->command->info("Routes Updated");
    }

    private function fillTemplate($routesTemplate, $routesUseTemplate, $name, $isAPI)
    {
        $modelName = Str::ucfirst($name);
        $routeName = strtolower($name);
        $auth = "->middleware('auth')";
        $controllerName = $isAPI ? $modelName . "APIController" : $modelName . "Controller";
        //Replace the templates
        if(!$isAPI){
            $routesTemplate = str_replace(
                ['{{modelName}}', '{{controllerName}}','{{auth}}'],
                [$routeName, $controllerName,$auth],
                $routesTemplate
            );
        }else{
            $routesTemplate = str_replace(
                ['{{modelName}}', '{{controllerName}}','{{auth}}'],
                [$routeName, $controllerName,''],
                $routesTemplate
            );
        }
        
        $routesUseTemplate = str_replace(
            '{{controllerName}}',
            $controllerName,
            $routesUseTemplate
        );
        //Get the old route
        $currRoute = file_get_contents(base_path('/routes/web.php'));
        if (Str::contains($currRoute, $modelName)) {
            $this->command->info("Routes already added, skipping adding routes");
            return $currRoute;
        } else {
            //Insert the use
            $currRoute = str_replace(
                '<?php',
                $routesUseTemplate,
                $currRoute
            );
            //Insert the route
            $currRoute .= "\n$routesTemplate";
            return $currRoute;
        }
    }
}
