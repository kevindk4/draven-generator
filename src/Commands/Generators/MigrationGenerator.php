<?php

namespace Draven\Generator\Commands\Generators;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class MigrationGenerator
{

    //* Command object */
    private $command;

    //Accept a command parameter
    public function __construct(Command $command)
    {
        $this->command = $command;
    }

    public function generate($name, $data, $soft_delete, $customPrimary)
    {
        $template = get_template("Migration");
        //Fill the template
        try {
            $result = $this->fillTemplate($template, $name, $data, $soft_delete, $customPrimary);
            $this->createFile($name, $result);
        } catch (Exception $e) {
            $this->command->error($e->getMessage());
            exit;
        }
    }

    private function createFile($name, $template)
    {
        $fileName = date('Y_m_d_His') . '_' . 'create_' . strtolower($name) . '_table.php';
        create_file(database_path('migrations/'), $fileName, $template);
        $this->command->comment("Migration $fileName Created");
    }

    private function fillTemplate($template, $name, $data, $soft_delete, $customPrimary)
    {

        //Generate up field
        $template = $this->generateUpField($template, $data, $soft_delete, $customPrimary);

        //replace tableName
        $template = $this->replaceTableName($template, $name);

        return $template;
    }

    private function generateUpField($template, $data, $soft_delete, $customPrimary)
    {
        $upField = "";

        //For Custom Primary
        if (isset($customPrimary)) {
            $upField .= "\$table->increments('$customPrimary');\n";
        } else {
            $upField .= "\$table->increments('id');\n";
        }

        //Fill The UP_FIELD
        foreach ($data as $count => $value) {
            if ($value->dbType == "") {
                $this->command->error("dbType can not be empty!");
                exit;
            }
            if (Str::contains($value->dbType, ",")) {
                $temp = explode(",", $value->dbType);
                $upField .= "\$table->$temp[0]('$value->fieldName',";
                foreach ($temp as $key => $value2) {
                    if ($key != 0) {
                        $upField .= $value2;
                        if ($key < sizeof($temp) - 1) {
                            $upField .= ",";
                        }
                    }
                }
                $upField .= ")";
            } else if ($value->dbType == 'enum' || $value->dbType == 'set') {
                if (isset($value->extra)) {
                    if (!is_array($value->extra)) {
                        $this->command->error("Extra have to be an array or null");
                        exit;
                    }
                    if (empty($value->extra)) {
                        $this->command->error("Extra value can not be empty!");
                        exit;
                    }
                    $upField .= "\$table->$value->dbType('$value->fieldName',[";
                    foreach ($value->extra as $key => $extra) {
                        if ($extra == "") {
                            $this->command->error("Extra value can not be empty!");
                            exit;
                        }
                        $upField .= "'$extra'";
                        if ($key < sizeof($value->extra) - 1) {
                            $upField .= ",";
                        }
                    }
                    $upField .= "])";
                } else {
                    $this->command->error("Extra value can not be empty!");
                    exit;
                }
            } else {
                $upField .= "\$table->$value->dbType('$value->fieldName')";
            }

            if (isset($value->dbOptions)) {
                if (!is_array($value->dbOptions)) {
                    $this->command->error("dbOptions have to be an array or null");
                    exit;
                }
                foreach ($value->dbOptions as $dbOptVal) {
                    if ($dbOptVal == "") {
                        break;
                    }
                    if (!Str::contains($dbOptVal, "(")) {
                        $upField .= "->$dbOptVal()";
                    } else {
                        $upField .= "->$dbOptVal";
                    }
                }
            }

            $upField .= ";\n";
        }

        foreach ($data as  $dataVal) {
            if (isset($dataVal->relation)) {
                foreach ($dataVal->relation as $count => $valueRel) {
                    if ($valueRel == "") {
                        break;
                    }
                    //For Foreign Key / Relationship
                    //Give foreign if relation type is mt1, 1t1
                    //Skip giving relation if relation type is mtm
                    //Add warning if type is mtm
                    if($valueRel->type == "mtm"){
                        $this->command->info("Application can not create migration for many to many relationship for now.");
                    }
                    //End of warning
                    if (($valueRel->type == "mt1" || $valueRel->type == "1t1") && $valueRel->type != "mtm") {
                        $fieldName = $dataVal->fieldName;
                        $otherModel = $valueRel->modelName;
                        $upField .= "\$table->foreign('$fieldName')->references('";
                        if (isset($valueRel->primary) && $valueRel->primary != "") {
                            $upField .= $valueRel->primary . "')";
                        } else {
                            $upField .= "id')";
                        }
                        $upField .= "->on('$otherModel');";
                        if ($count < sizeof($dataVal->relation) - 1) {
                            $upField .= "\n";
                        }
                    } else {
                        continue;
                    }
                }
            }
        }


        //For Soft Delete
        if ($soft_delete) {
            $upField .= "\$table->softDeletes();\n";
        }

        //For timestamp
        $upField .= "\$table->timestamps();\n";

        //End of fill the UP_FIELD
        $template = str_replace(
            ['{{UP_FIELD}}'],
            [$upField],
            $template
        );

        return $template;
    }

    private function replaceTableName($template, $name)
    {
        $tableName = Str::lower($name);
        $className = Str::ucfirst($name);
        $template = str_replace(
            ['{{tableName}}', '{{className}}'],
            [$tableName, $className],
            $template
        );

        return $template;
    }
}
