<?php

namespace Draven\Generator\Commands;

use Draven\Generator\Commands\Generators\ModelGenerator;
use Illuminate\Console\Command;
use Exception;

class ModelGeneratorCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'draven:model {model : model name} 
    {--primary= : Custom primary key, will be created as auto-increment integer} 
    {--fromFile= : Load model field from a JSON file} 
    {--view : create the views}
    {--migration : create migration}
    {--repository : create with repository pattern}
    {--route : generate route}
    {--api : is a model for API?}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a model class';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = $this->argument('model');
        $customPrimary = $this->option('primary');
        $fromFile = $this->option('fromFile');
        $view = $this->option('view');
        $migration = $this->option('migration');
        $repository = $this->option('repository');
        $route = $this->option('route');
        $api = $this->option('api');
        $inputs = array();
        $relation = [];
        $soft_delete = false;

        //Create model_json directory
        create_directory(resource_path('model_json'));

        //For Soft Delete
        if ($this->confirm("Do you want this model to use soft delete?", true)) {
            $soft_delete = true;
        }
        if (isset($fromFile)) {
            try {
                if (file_exists($fromFile)) {
                    $filePath = $fromFile;
                } elseif (file_exists(base_path($fromFile))) {
                    $filePath = base_path($fromFile);
                } else {
                    $filePath = resource_path('model_json/') . $fromFile;
                }

                if (!file_exists($filePath)) {
                    $this->error('JSON file not found');
                    exit;
                }

                $fileContent = file_get_contents($filePath);
                $inputs = json_decode($fileContent);
            } catch (Exception $e) {
                $this->error($e->getMessage());
                exit;
            }
        } else {
            while (true) {
                $dataPush = [];

                //For Field
                $field = $this->ask('Enter field name or "exit" to exit the command');
                if ($field == '') {
                    $this->error('Field can not be an empty string!');
                    continue;
                }
                if ($field == "exit") break;

                //For field type
                $type = $this->ask('Enter field type ex: integer,string,boolean,etc.');
                if ($type == '') {
                    $this->error('Type can not be empty!');
                    continue;
                }
                
                //For HTML Type
                $HTMLtype = $this->ask('Enter html type ex: text,number,checkbox,etc. (ref:https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input)');
                if ($HTMLtype == '') {
                    $this->error('HTML Type can not be empty!');
                    continue;
                } 


                //For Database Type
                $dbType = $this->ask("Enter field database type ex:\"integer\",\"string,25\",\"double,8,2\",etc.\n (ref : https://laravel.com/docs/8.x/migrations#available-column-types)");
                if (strtolower($dbType) == 'enum' || strtolower($dbType) == 'set') {
                    $extra = $this->ask("Enter $dbType fields ex:\"easy,hard\"");

                    if ($extra == '') {
                        $this->error("Extra can not be empty!");
                        continue;
                    }
                }

                if ($dbType == '') {
                    $this->error('Database type can not be empty!');
                    continue;
                }

                //Database options
                $this->comment("Leave empty if not used");
                $dbOptions = $this->ask("Enter modifier for the database type , ex: \"unique,unsigned,charset('utf8mb4'),default('defaultVal')\"\n (ref : https://laravel.com/docs/8.x/migrations#column-modifiers)");


                if (isset($field) && isset($type) && isset($dbType) && isset($HTMLtype)) {
                    $dataPush['fieldName'] = $field;
                    $dataPush['fieldType'] = $type;
                    $dataPush['htmlType'] = $HTMLtype;
                    $dataPush['dbType'] = $dbType;
                    if (isset($extra)) {
                        $dataPush['extra'] = explode(",", $extra);
                    }
                    if (isset($dbOptions)) {
                        $dataPush['dbOptions'] = explode(",", $dbOptions);
                    }
                }

                //For Validation
                $validation = $this->ask("Enter Validation (ex: \"required\",\"min:5\",etc.)");
                if (isset($validation)) {
                    $dataPush['validation'] = $validation;
                }


                //Relationship
                if ($this->confirm("Do you want to add relationship?")) {
                    while (true) {
                        $this->comment("Type \"end\" to exit relation");
                        $this->info("Many to many relationship is not supported in this version.");
                        $relationType = $this->ask("1t1 : 1 to 1,\n 1tm : 1 to many,\n mt1 : many to 1, \n mtm : many to many\n Relation type from this model to another model :");
                        if($relationType == "end"){
                            break;
                        }
                        if ($relationType != "1t1" && $relationType != "mt1" && $relationType != "1tm" && $relationType != "mtm") {
                            $this->error("Unrecognized relation type, skipping relationship.");
                            continue;
                        }
                        $modelName = $this->ask("Enter the foreign model name : ex:\"ModelName\"");
                        $oModelPrimary = $this->ask("Enter foreign model primary key (leave empty if the primary key is id) :");
                        if (isset($relationType) && isset($modelName)) {
                            $relation[] = (object)[
                                'type' => $relationType,
                                'modelName' => $modelName
                            ];
                            //Handle Append
                            $lastObject = $relation[sizeof($relation)-1];
                            if ($relationType == "1t1" || $relationType == "mt1") {
                                if ($this->confirm("Do you want to use append?")) {
                                    while (true) {
                                        $this->comment("Type \"end\" to exit append");
                                        $attrName = $this->ask("Enter foreign table attribute name");
                                        if ($attrName == "end") {
                                            break;
                                        }
                                        if (isset($attrName)) {
                                            $lastObject->append[] = $attrName;
                                        } else {
                                            $this->error("foreign table attribute name can not be empty!");
                                            continue;
                                        }
                                    }
                                }
                            }
                            //Handle Custom Foreign Primary
                            if (isset($oModelPrimary)) {
                                $lastObject->primary = $oModelPrimary;
                            }
                            $dataPush['relation'] = $relation;
                        } else {
                            $this->error("One of the input is empty, skipping relationship");
                        }
                    }
                }

                array_push($inputs, (object)$dataPush);
            }
        }

        //Model Generator
        $modelGenerator = new ModelGenerator($this);
        $modelGenerator->generate($name, $inputs, $soft_delete, $customPrimary, $view, $migration, $repository, $route, $api);
    }
}
