<?php

namespace Tests;

use PHPUnit\Framework\TestCase;

class CRUDGeneratorTest extends TestCase
{
    public function testModel()
    {
        $modelNameUCF = "Car";

        //Test Model Generation
        $this->assertTrue(file_exists("app/Models/" . $modelNameUCF . ".php"));
    }

    public function testController()
    {
        $modelNameUCF = "Car";
        //Test Controller Generation
        $this->assertTrue(file_exists("app/Http/Controllers/" . $modelNameUCF . "Controller.php"));
    }

    public function testRepository()
    {
        $modelNameUCF = "Car";
        $this->assertTrue(file_exists("app/Repositories/" . $modelNameUCF . "Repository.php"));
        $this->assertTrue(file_exists("app/Repositories/BaseRepository.php"));
    }

    public function testRequest()
    {
        $modelNameUCF = "Car";
        $this->assertTrue(file_exists("app/Http/Requests/" . $modelNameUCF . "Request.php"));
    }

    public function testMigration()
    {
        $modelName = "car";
        $migrationDir = scandir("database/migrations");
        $migrationRes = false;
        foreach ($migrationDir as $fileName) {
            if (strpos($fileName, $modelName) !== false) {
                $migrationRes = true;
            }
        }
        $this->assertTrue($migrationRes);
    }

    public function testRoute()
    {
        $modelName = "car";
        $route = file_get_contents("routes/web.php");
        $routeRes = false;
        if (strpos($route, $modelName) !== false) {
            $routeRes = true;
        }
        $this->assertTrue($routeRes);
    }

    public function testViewLayout()
    {
        //Test Layouts
        $this->assertTrue(file_exists("resources/views/layouts/app.blade.php"));
        $this->assertTrue(file_exists("resources/views/layouts/bootstrap_incl.blade.php"));
        $this->assertTrue(file_exists("resources/views/layouts/menu.blade.php"));
        $this->assertTrue(file_exists("resources/views/layouts/sidebar.blade.php"));

        //Test Home Page
        $this->assertTrue(file_exists("resources/views/home.blade.php"));
    }

    public function testViewModel()
    {
        $modelName = "car";
        //Test Model Page
        $this->assertTrue(file_exists("resources/views/" . $modelName . "/create.blade.php"));
        $this->assertTrue(file_exists("resources/views/" . $modelName . "/edit.blade.php"));
        $this->assertTrue(file_exists("resources/views/" . $modelName . "/fields.blade.php"));
        $this->assertTrue(file_exists("resources/views/" . $modelName . "/index.blade.php"));
        $this->assertTrue(file_exists("resources/views/" . $modelName . "/table.blade.php"));
    }
}
