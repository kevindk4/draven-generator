<?php

namespace Draven\Generator;

use Draven\Generator\Commands\APIControllerCommand;
use Draven\Generator\Commands\APIGeneratorCommand;
use Draven\Generator\Commands\ControllerGeneratorCommand;
use Draven\Generator\Commands\CRUDGeneratorCommand;
use Draven\Generator\Commands\MigrationGeneratorCommand;
use Draven\Generator\Commands\ModelGeneratorCommand;
use Draven\Generator\Commands\RepositoryGeneratorCommand;
use Draven\Generator\Commands\RequestGeneratorCommand;
use Draven\Generator\Commands\ViewGeneratorCommand;
use Illuminate\Support\ServiceProvider;

class DravenGeneratorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('draven:generate', function ($app) {
            return new CRUDGeneratorCommand();
        });

        $this->app->singleton('draven:model', function ($app) {
            return new ModelGeneratorCommand();
        });

        $this->app->singleton('draven:controller', function ($app) {
            return new ControllerGeneratorCommand();
        });

        $this->app->singleton('draven:repository', function ($app) {
            return new RepositoryGeneratorCommand();
        });

        $this->app->singleton('draven:request', function ($app) {
            return new RequestGeneratorCommand();
        });

        $this->app->singleton('draven:view', function ($app) {
            return new ViewGeneratorCommand();
        });
        
        $this->app->singleton('draven:migration', function ($app) {
            return new MigrationGeneratorCommand();
        });

        $this->app->singleton('draven:api', function ($app) {
            return new APIGeneratorCommand();
        });
        
        $this->app->singleton('draven:api.controller', function ($app) {
            return new APIControllerCommand();
        });

        $this->commands([
            'draven:generate',
            'draven:model',
            'draven:controller',
            'draven:repository',
            'draven:request',
            'draven:view',
            'draven:migration',
            'draven:api',
            'draven:api.controller'
        ]);
    }
}
